<?php
    /*
     * Opgave 03_06
     * 
     * Metoden getEventById skal kunne lokalisere en event ud fra id.
     * Når metoden har fundet det array der indeholder den specifikke instans, skal metoden returnere dette array.
     * HJÆLP: Med en foreach løkke, kan du løbe arrayet igennem. For hvert gennemløb kan du evaluere, om der en en nøgle med en bestemt værdi.
     * Se kapitel - Array -> Traversing Arrays
     */
    
    class Event
    {
        private $events = array(
            array(
            "EventId"=>1,
            "EventName"=>"Rave party",
            "EventDescription"=>"For young people",
            "EventDate"=>"Oktober 1 2016 10:00pm",
            "Lat"=>"56.4",
            "Long"=>"9",
            "EventImage"=>"img/rave.png"
        ),
        array(
            "EventId"=>2,
            "EventName"=>"Opera",
            "EventDescription"=>"For not so young people",
            "EventDate"=>"Oktober 2 2016 10:00pm",
            "Lat"=>"56.3",
            "Long"=>"9.4",
            "EventImage"=>"img/opera.png"
        ),
        array(
            "EventId"=>3,
            "EventName"=>"Metal",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3",
            "EventImage"=>"img/metal.png"
        ));
        function __construct()
        {
        }

        function getEventById($EventId){
            $container = "";

            foreach ($this->events as $eve) { 
                if(is_array($eve)){
                    if($eve["EventId"] == $EventId){ 
                        
                        foreach($eve as $key=>$val){
                            $container .= "<li>" . "Keyværdi: " . $key . " Value: " . $val . "</li>"; // tilføjer keys og values i container
                        }  
                    } 
                }else{ 
                    $container .= "WRONG!";
                }
           
            }
            return $container;
        }
    }

$event = new Event;
echo $event->getEventById(1);
?>