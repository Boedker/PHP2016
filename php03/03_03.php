<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden getAllValuesFromAnArray skal kunne finde værdierne i et associativt array.
     * Brug den indbyggede metode array_keys().
     * Se kapitel - Array -> Extracting multiple values og afsnittet Keys and values
     */
    
    class Event
    {
        function getAllValuesFromAnArray()
        {
            $container = ""; // tom $container der bruges senere
            $event = array( // her angiver vi vores array
            "EventId"=>3,
            "EventName"=>"Metal",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3", // her er "long" key med den tilhørende value "9.3"
            "EventImage"=>"img/metal.png"
            );
        foreach ($event as $key => $value){ // foreach siger at for hver $key med en $value gør følgende
            $container .= $key . " " . $value . "<br>"; // variablen $container tilføjes en $key og en $value og en html <br>. Det sker hver gang det overstående er sandt.
        }
        return $container; // retunere $container der nu indeholder en masse $keys og $values fra foreach løkken
       // var_dump(array_values($event)); // array_keys bruges til at finde key-værdier, array_values bruges til at finde værdierne
      
        }
        
    }
    $event = new Event;
    echo $event->getAllValuesFromAnArray();
?>