<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden extractValuesFromArray skal kunne hente værdierne fra et indexeret array. 
     * Brug den indbyggede metode list() til at udtrække værdierne.
     * Se kapitel - Array -> Extracting multiple values.
     */
    
    class Person
    {
        function extractValuesFromArray()
        {
            $someRandomPerson = array("Fred", 35, "Betty");
            list($name, $age, $wife) = $someRandomPerson; //$name is "Fred", $age is 35, $wife is "Betty"
            //return [$name, $age, $wife]
            return $name . " " . $age . " " . $wife;
        }
    }
    
    $person = new Person; // variablen $person kalder nu et object person. Aktiveres her ved at skrive variablen $person
    echo $person->extractValuesFromArray(); //udskriver hvad der bliver retuneret fra funktionen i objektet
    
?>  