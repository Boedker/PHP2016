<?php
    /*
     * Opgave 03_02
     * 
     * Metoden metoden removePersonFromArray skal kunne fjerne en person fra et indexeret array.
     * Brug den indbyggede metode array_slice().
     * Se kapitel - Array -> Extracting multiple values og afsnittet Slicing Array
     */
    
    class Person
    {
        function removePersonFromArray()
        {
            $people = array("Tom", "Dick", "Harriet", "Brenda", "Jo");
            $newPeopleArray = array_slice($people, 0, 4); // array_slice skærer en person væk. Returnerer de personer der ikke er skåret væk. Første tal er start positon, andet tal er hvor mange den skal fjerne derfra
            list($first, $second, $third, $fourth) = $newPeopleArray;
            //unset($newPeopleArray[2]); //Fjerner specifikt plads nr. 2. Beholder samme array (newPeopleArray)
            //var_dump($newPeopleArray);
            return $first . " " . $second . " " . $third . " " . $fourth; //returnerer variablerne
        }
    }
    $person = new Person;
    echo $person->removePersonFromArray();
?>