<?php

//Du skal lave et nyt kort der kun viser en enkelt makør baseret på input fra brugeren.
//En funktionsdefinition kunne se således ud:

class Event
    {
        private $events = array(
            array(
            "EventId"=>1,
            "EventName"=>"Rave party",
            "EventDescription"=>"For young people",
            "EventDate"=>"Oktober 1 2016 10:00pm",
            "Lat"=>"56.4",
            "Long"=>"9",
            "EventImage"=>"img/rave.png"
        ),
        array(
            "EventId"=>2,
            "EventName"=>"Opera",
            "EventDescription"=>"For not so young people",
            "EventDate"=>"Oktober 2 2016 10:00pm",
            "Lat"=>"56.3",
            "Long"=>"9.4",
            "EventImage"=>"img/opera.png"
        ),
        array(
            "EventId"=>3,
            "EventName"=>"Metal",
            "EventDescription"=>"For everybody",
            "EventDate"=>"Oktober 2 2016 2:00am",
            "Lat"=>"56.4",
            "Long"=>"9.3",
            "EventImage"=>"img/metal.png"
        ),
        array(
            "EventId"=>4,
            "EventName"=>"Limfjorden Rundt Træskibssejlads",
            "EventDescription"=>"Turen går Løgstør, Thisted, Struer, Nykøbing Mors, Fur og Skive.",
            "EventDate"=>"September 12 2016 12:00am",
            "Lat"=>"56.9",
            "Long"=>"9.24",
            "EventImage"=>"img/limfjorden.png"
        ),
        array(
            "EventId"=>5,
            "EventName"=>"Oyster Trophy Week",
            "EventDescription"=>"Under østersfestivalen Oyster Trophy Week finder du østersarrangementer i alle afskygninger",
            "EventDate"=>"Oktober 11 2016 12:00am",
            "Lat"=>"56.7",
            "Long"=>"8.86",
            "EventImage"=>"img/Oyster.png"
        ));
        function getEventById($EventId)
        {
            $container = '';
            foreach($this->events as $eve){
                if(is_array($eve)){
                    if($eve["EventId"] == $EventId){
                        foreach($eve as $key=>$val){
                                $container .= "L.marker([" . $eve["Lat"] . "," . $eve["Long"]  . "]).addTo(mymap).bindPopup('" . $eve["EventDescription"] . "'); \n";
                            }  
                        }  
                }else{
                    echo "out";
                }
            }
             return $container;
            //Denne funktion skal returnere alle event i events arrayet
        }
    }
//Du skal benytte det samme array fra forrige opgave.

?>