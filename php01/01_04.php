<?php
    // Datatyper og variabler. Dette dokument indlejres i det efterfølgende

    //tekststreng (string)
    $firstname = "Mia";

    //tekststreng (string)
    $lastname = "Nissen";

    //nummerisk heltal (integer)
    $age = 24;

    //boolean (sandt/falsk)
    $inRelationship = true;

    //tekststreng (string)
    $work = "Student";

    //tekststreng (string)
    $workPlace = "Erhversakademi Dania";

    //array (en række)
    $hobbies = ["Tegning, bøger, film og spil"];

 ?>