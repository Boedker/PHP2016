<!doctype>

<head>
    <title>Min kontaktside</title>
    <meta charset="utf-8">
    <!-- Reference til bootstrap -->
    <?php
    include("data.php");
    ?>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <h2>Resume</h2>
                    <?php
                        echo "Navn: ".$firstname . " " . $lastname. "<br>";
                        echo "Alder: ".$age . "<br>";
                        echo "Email: ".$email . "<br>";
                        echo "Mobil: ".$phone;
                    ?>
            </div>
            <div class="col-md-3">
                <h2>Præferencer</h2>
                    <?php
                        foreach ($preferences as $value){
                            echo "$value <br>";
                        }
                    ?>
            </div>
            <div class="col-md-3">
                <h2>Portefølje!</h2>
                    <?php
                        foreach ($jobs as $location => $descriptsion){
                            echo "Hos " . $location . "  lavede jeg " . $descriptsion. "<br>";
                        }
                    ?>
            </div>
            <div class="col-md-3">
                <h2>Kompetencer</h2>
                    <?php
                        //echo $competencies[0]. "<br>";
                        //echo $competencies[1][0]. ", ". $competencies[1][1]. ", ". $competencies[1][2]. "<br>";
                        //echo $competencies[2][0]. ", ". $competencies[2][1]. ", ". $competencies[2][2]. "<br>";

                    ?>
                    <?php
                        foreach ($competencies as $comp) {  //foreach løkken henter variablen $competencies - giver den navnet $comp
                            if(is_array($comp)){            // if - hvis er array indeholder et array gør den således
                                foreach ($comp as $c) {     // for være gang $comp optræder så udskriver den $c
                                    echo "<li>" . $c . "</li>";
                                }
                            }else {
                                echo "<li>" . $comp . "</li>";
                            }
                        }
                    ?>
            </div>
        </div>
    </div>
<body>