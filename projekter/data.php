<?php
    /* Dette dokument skal indeholde de dataelementer der indgår i html siden.
     * Følgende dataelementer skal som minimum være udpenslet i dette dokument.:
     * - email 
     * - phone
     * - preferences
     * - jobs
     * - competencies
     *
     * Det vil være naturligt at lade ovenstående elementer være 
     */
     $firstname = "Mia Bødker";
     $lastname = "Nissen";
     $age = 24;
     $email = "miaboedkernissen@gmail.com";
     $phone = 30914882;

     $preferences = array('Tegning','Photoshop','Fotografering og computerspil');

     $jobs = array('Værtindeshoppen' => 'Ungarbejder','Italiano is og pastahuset' => 'Opvasker og tjener','Superbrugsen' => 'Salgsassistent','Boedker Illustrations' => 'Illustrator og concept artist');

     
     $competencies = array('Tegning',array('Klassisk tegning','digital tegning','concept tegning','karakter udvikling'), array('Photoshop','PaintTool Sai','MediBang paint pro'));

?>