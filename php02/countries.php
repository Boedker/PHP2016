<!doctype>

<head>
    <title>Min kontaktside</title>
    <meta charset="utf-8">
    <!-- Reference til bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
     <?php
        $countries = array( "Italy"=>"Rome", "Luxembourg"=>"Luxembourg", "Belgium"=>"Brussels", "Denmark"=>"Copenhagen", "Finland"=>"Helsinki", "France"=>"Paris", "Slovakia"=>"Bratislava", "Slovenia"=>"Ljubljana", "Germany"=>"Berlin", "Greece"=>"Athens", "Ireland"=>"Dublin", "Netherlands"=>"Amsterdam", "Portugal"=>"Lisbon", "Spain"=>"Madrid", "Sweden"=>"Stockholm", "United Kingdom"=>"London", "Cyprus"=>"Nicosia", "Lithuania"=>"Vilnius", "Czech Republic"=>"Prague", "Estonia"=>"Tallin", "Hungary"=>"Budapest", "Latvia"=>"Riga", "Malta"=>"Valetta", "Austria"=>"Vienna", "Poland"=>"Warsaw");

        $countries += array("Vietnam"=>"Hanoi", "Korea"=>"Seoul");
        //var_dump($countries);
       
    ?>


<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <h2>Lande</h2>
                    <?php
                         foreach ($countries as $key => $value) {
            echo "<li> Land: " . $key . ", Hovedstaden: " . $value . "</li>";
                         }
                    ?>
            </div>
            <div class="col-md-4">
                <h2>Alfabetisk efter byer</h2>
                    <?php
                      
                     asort($countries);
                     foreach ($countries as $key => $value){
                     echo "<li> Land: " . $key . ", Hovedstaden: " . $value . "</li>";

        }
                    ?>
            </div>
            <div class="col-md-4">
                <h2>Alfabetisk efter lande</h2>
                    <?php
                     
                    ksort($countries);
                    foreach ($countries as $key => $value){
                    echo "<li> Land: " . $key . ", Hovedstaden: " . $value . "</li>";

        }
                    ?>
            </div>
        </div>
    </div>
<body>


